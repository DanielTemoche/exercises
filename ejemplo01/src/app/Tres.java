package app;
public class Tres extends Thread {
    String nombre;
    public Tres (int prioridad,String nombre){
    this.nombre=nombre;
    setPriority(prioridad);
   }
    public void run(){
    for(int c=1;c<=50;c++){
    System.out.print(c+"mts ");
    yield(); 
    }
    System.out.println("\n Llego " + nombre);
   }
    static Tres Mario;
    static Tres Rodrigo;
    static Tres Jose;
    public static void main(String []args) throws InterruptedException {
    Rodrigo = new Tres(1," Rodrigo ");
    Mario = new Tres(3," Mario ");
    Jose = new Tres(5," Jose ");
    Rodrigo.start();
    Mario.start();
    Jose.start();
    Rodrigo.join();
    Mario.join();
    Jose.join();
    }
   }