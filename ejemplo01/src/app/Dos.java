/*Mostrar los números del 1 al 25, con un retardo de tiempo de 2000 mili-
segundos.*/

package app;

public class Dos {
    public static void main(String[] args) throws InterruptedException {
        int i = 1;
        System.out.println("Numeros del 1 al 25: ");
        do{
                   System.out.println(i);
                   
                   i++;
                   Thread.sleep(2000);
        }while(i<=25);
    }
    
}