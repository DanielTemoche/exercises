package app;

import java.util.Scanner;

public class App extends Thread{

    Scanner datos = new Scanner(System.in);
    String nombre, dia;
    double hora;
    public App(String nombre, String dia, double hora){
        this.nombre = nombre;
        this.dia=dia;
        this.hora=hora;

        System.out.println("ingrese nombre:");
        nombre=datos.nextLine();
        System.out.println("ingrese el dia:");
        dia=datos.nextLine();
        System.out.println("ingrese la hora: ");
        hora=datos.nextDouble();
    }
    @Override
    public void run(){
        if (hora>13) {
            System.out.println(nombre+"llego tarde el dia: "+ dia);
        }else{
            System.out.println(nombre+ "llego temprano el dia: "+dia);
        }
    }
    

    public static void main(String[] args) throws Exception {
        Thread alumno1 = new App("", "", 0);
        alumno1.start();
        Thread.sleep(3000);
        
        Thread alumno2 = new App("", "", 0);
        alumno2.start();
        Thread.sleep(3000);

        System.out.println("Hello Java");
    }
}